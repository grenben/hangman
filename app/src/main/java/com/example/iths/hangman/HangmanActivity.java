package com.example.iths.hangman;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

/**
 * <h1HangmanActivity</h1>
 * HangmanActivity is the activity that controls and render the Hangman Game view.
 *
 * @author Johan Grenlund - ITHS.se
 * @version 2015.10.06
 * @since 1.0
 */

/**
 * <h3>HangmanActivity</h3>
 * HangmanActivity class extends AppCompatActivity
 *
 */
public class HangmanActivity extends AppCompatActivity {


    /**
     * GET_HIDDEN_WORD
     * String key for Intent
     */
    public static final String GET_HIDDEN_WORD = "GET_HIDDEN_WORD";
    /**
     * GET_USED_LETTERS
     * String key for Intent
     */
    public static final String GET_USED_LETTERS = "GET_USED_LETTERS";
    /**
     * GET_WORD
     * String key for Intent
     */
    public static final String GET_BAD_USED_LETTERS = "GET_BAD_USED_LETTERS";
    /**
     * GET_HIDDEN_WORD
     * String key for Intent
     */
    public static final String GET_WORD = "GET_WORD";
    /**
     * GET_TRIES_LEFT
     * String key for Intent
     */
    public static final String GET_TRIES_LEFT = "GET_TRIES_LEFT";
    /**
     * GET_VISIBLE_WORD
     * String key for Intent
     */
    public static final String GET_VISIBLE_WORD = "GET_VISIBLE_WORD";
    /**
     * RESULT
     * String key for Intent
     */
    public static final String RESULT = "RESULT";
    /**
     * TRIES_LEFT
     * String key for Intent
     */
    public static final String TRIES_LEFT = "TRIES_LEFT";
    /**
     * HIDDEN_WORD
     * String key for Intent
     */
    public static final String HIDDEN_WORD = "HIDDEN_WORD";

    private HangmanModel h;
    private EditText guessedTextField;
    private TextView hiddenWordTextView;
    private ImageView hangmanImage;
    private TextView triesLeftTextView;
    private TextView usedLetterTextView;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (savedInstanceState != null) {
            h = new HangmanModel();
            h.setUsedLetters(savedInstanceState.getStringArrayList(GET_USED_LETTERS));
            h.setBadLettersUsed(savedInstanceState.getStringArrayList(GET_BAD_USED_LETTERS));
            h.setTriesLeft(savedInstanceState.getInt(GET_TRIES_LEFT));
            h.setWord(savedInstanceState.getString(GET_WORD));
            h.setVisibleWord(savedInstanceState.getStringArrayList(GET_VISIBLE_WORD));

        } else {
            h = new HangmanModel();

        }
        setContentView(R.layout.activity_hangman);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        try {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        } catch (NullPointerException e) {
            Toast.makeText(getApplicationContext(), R.string.get_support_action_bar_error_toast, Toast.LENGTH_SHORT).show();
        }

        guessedTextField = (EditText) findViewById(R.id.guessed_letter);
        hiddenWordTextView = (TextView) findViewById(R.id.hangman_hidden_word);
        hangmanImage = (ImageView) findViewById(R.id.hangman_image);
        triesLeftTextView = (TextView) findViewById(R.id.tries_left_text_view);
        usedLetterTextView = (TextView) findViewById(R.id.used_letters_text_view);

        hiddenWordTextView.setText(h.getHiddenWord());

        triesLeftTextView.setText(triesLeftStringFormatter());

        if(!h.getBadLettersArray().isEmpty()) {
            usedLetterTextView.setText(h.badLettersUsed());
        }
        imageUpdater();

    }


    @Override
    protected void onSaveInstanceState(Bundle savedInstanceState) {
        savedInstanceState.putInt(GET_TRIES_LEFT, h.getTriesleft());
        savedInstanceState.putString(GET_HIDDEN_WORD, h.getHiddenWord());
        savedInstanceState.putStringArrayList(GET_USED_LETTERS, h.getUsedLettersArray());
        savedInstanceState.putStringArrayList(GET_BAD_USED_LETTERS, h.getBadLettersArray());
        savedInstanceState.putString(GET_WORD, h.getRealWord());
        savedInstanceState.putStringArrayList(GET_VISIBLE_WORD, h.getVisibleWord());
        super.onSaveInstanceState(savedInstanceState);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.hangman_activity_actions, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_info_button) {
            Intent intentInfo = new Intent(this, InfoActivity.class);
            startActivity(intentInfo);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    /**
     * <h3>guessButtonPressed</h3>
     * Checks if user has inputted not more than one letter, if user has inputted letter before
     * and if user has made a input. Throws an IndexOutOfBoundsException if no letter is guessed.
     * Displays a Toast with error corresponding message.
     * Uppdates Hangman image and all textviews.
     * Checks if player has won or lost the game.
     * When player has won or lost the game player will be sent to result activity.
     *
     * @param view current view
     */
    public void guessButtonPressed(View view) {
        String guessedLetter = guessedTextField.getText().toString();

        int length = guessedTextField.getText().length();
        try {
            if (length > 1) {
                Toast.makeText(getApplicationContext(), R.string.you_can_only_guess_one_letter_toast, Toast.LENGTH_SHORT).show();

            } else if (h.hasUsedLetter(guessedTextField.getText().toString())) {
                Toast.makeText(getApplicationContext(), R.string.you_have_used_this_letter_toast, Toast.LENGTH_SHORT).show();

            } else if (!h.hasUsedLetter(guessedTextField.getText().toString())){
                h.guess(guessedLetter);

            }
        } catch (IndexOutOfBoundsException  e) {
            Toast.makeText(getApplicationContext(), R.string.you_have_not_guessed_letter_toast, Toast.LENGTH_SHORT).show();

        }
        guessedTextField.setText("");
        triesLeftTextView.setText(triesLeftStringFormatter());
        hiddenWordTextView.setText(h.getHiddenWord());
        usedLetterTextView.setText(h.badLettersUsed());

        imageUpdater();

        if(h.hasWon()) {
            didWin();

        } else if (h.hasLost()) {
           didLost();
        }

    }

    private void didLost() {
        Intent intent = new Intent(this, ResultActivity.class);
        String hiddenWord = h.getRealWord();
        int triesLeft = h.getTriesleft();
        String didLoose = getString(R.string.you_did_loose_message);
        intent.putExtra(RESULT, didLoose);
        intent.putExtra(TRIES_LEFT, triesLeft);
        intent.putExtra(HIDDEN_WORD, hiddenWord);
        startActivity(intent);
        finish();
    }

    private void didWin() {
        Intent intent = new Intent(this, ResultActivity.class);
        String hiddenWord = h.getHiddenWord();
        int triesLeft = h.getTriesleft();
        String didWon = getString(R.string.you_did_win_message);
        intent.putExtra(RESULT, didWon);
        intent.putExtra(TRIES_LEFT, triesLeft);
        intent.putExtra(HIDDEN_WORD, hiddenWord);

        startActivity(intent);
        finish();

    }

    private void imageUpdater() {
        int nextImage = h.getTriesleft();

        switch (nextImage) {
            case 1: hangmanImage.setImageResource(R.drawable.hang1);
                break;
            case 2: hangmanImage.setImageResource(R.drawable.hang2);
                break;
            case 3: hangmanImage.setImageResource(R.drawable.hang3);
                break;
            case 4: hangmanImage.setImageResource(R.drawable.hang4);
                break;
            case 5: hangmanImage.setImageResource(R.drawable.hang5);
                break;
            case 6: hangmanImage.setImageResource(R.drawable.hang6);
                break;
            case 7: hangmanImage.setImageResource(R.drawable.hang7);
                break;
            case 8: hangmanImage.setImageResource(R.drawable.hang8);
                break;
            case 9: hangmanImage.setImageResource(R.drawable.hang9);
                break;
            case 10: hangmanImage.setImageResource(R.drawable.hang10);
                break;
            default: hangmanImage.setImageResource(R.drawable.hang0);

        }
    }

    private String triesLeftStringFormatter() {
        String triesLeftTextViewString;
        return triesLeftTextViewString = String.format(getResources().getString(R.string.you_have_guess_left_text), h.getTriesleft());

    }
}
