package com.example.iths.hangman;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * Created by iths on 15-10-24.
 * <h1>Hangman class</h1>
 * Hangman class has a list of words and methods controlling the logic of a classic Hangman game.
 *
 * @author Johan Grenlund - ITHS.se
 * @version 2015.10.06
 * @since 1.0
 */
public class HangmanModel {

    private String[] wordList = { "computer", "java", "activity", "alaska",
                    "appearance", "javatar", "automobile", "falafel", "birthday",
                    "canada", "central", "character", "chicken", "chosen", "cutting",
                    "daily", "darkness", "shawarma", "disappear", "driving", "effort",
                    "establish", "exact", "establishment", "fifteen", "football",
                    "foreign", "frequently", "frighten", "function", "gradually",
                    "hurried", "identity", "importance", "impossible", "invented",
                    "italian", "journey", "lincoln", "london", "massage", "minerals",
                    "outer", "paint", "particles", "personal", "physical", "progress",
                    "quarter", "recognise", "replace", "rhythm", "situation",
                    "slightly", "steady", "stepped", "strike", "successful", "sudden",
                    "terrible", "traffic", "unusual", "volume", "yesterday" };

    private int whatWord;
    private String word;
    private int triesLeft;
    private ArrayList<String> usedLetters = new ArrayList<>();
    private ArrayList<String> badLettersUsed = new ArrayList<>();
    private boolean hasLost = false;
    private boolean hasWon = false;
    private ArrayList<String> visibleWord = new ArrayList<>();

    /**
     * <h3>HangmanModel</h3>
     * Constructor of the class HangmanModel.
     * Randomizes a word from word list.
     * Sets number of tries.
     * Makes a hidden word with equal lenght as word.
     *
     */
    public HangmanModel() {
        triesLeft = 10;
        whatWord = (int) (Math.random() * wordList.length);
        word = wordList[whatWord];

        for (int i=0; i<wordList[whatWord].length(); i++) {
            visibleWord.add("-");
        }

    }

    /**
     * getVisibleWord
     * Returns an ArrayList with letter Strings from the visible word.
     *
     * @return visibleWord in an ArrayList of strings
     */
    public ArrayList<String> getVisibleWord() {
        return visibleWord;
    }

    /**
     * setVisibleWord
     * Sets the visibleWord ArrayList
     *
     * @param visibleWord as an ArrayList
     */
    public void setVisibleWord(ArrayList<String> visibleWord) {
        this.visibleWord = visibleWord;
    }

    /**
     * getBadLettersArray
     * Returns an ArrayList of letters in String format with bad letters used.
     *
     * @return badLettersUsed in ArrayList format
     */
    public ArrayList<String> getBadLettersArray() {
        return badLettersUsed;
    }

    /**
     * setBadLettersUsed
     * Sets setBadLettersUsed ArrayList
     *
     * @param badlettersToArray as an ArrayList
     */
    public void setBadLettersUsed(ArrayList<String> badlettersToArray) {
        this.badLettersUsed = badlettersToArray;
    }

    /**
     * getUsedLettersArray
     * Returns used letters in an ArrayList of letters in String format.
     *
     * @return usedLetters as an arraylist of strings
     */
    public ArrayList<String> getUsedLettersArray() {
        return usedLetters;
    }

    /**
     * setUsedLetters
     * Sets used letters in an ArrayList of strings
     *
     * @param usedLettersToArray ArrayList of strings
     */
    public void setUsedLetters(ArrayList<String> usedLettersToArray) {
        this.usedLetters = usedLettersToArray;
    }

    /**
     * setTriesLeft
     * Sets number of tries left.
     *
     * @param triesLeft in int format
     */
    public void setTriesLeft(int triesLeft) {
        this.triesLeft = triesLeft;
    }

    /**
     * getRealWord
     * Returns the current word.
     *
     * @return word in string format
     */
    public String getRealWord() {
        return word;
    }

    /**
     * setWord
     * Sets the current word.
     *
     * @param word in string format
     */
    public void setWord(String word) {
        this.word = word;
    }

    /**
     * getHiddenWord
     * Returns the hiddenword in a string
     * Checks if letter is in usedLetters arrayList. If so, method reveals that letter from hidden word.
     *
     * @return listToString hidden word in a string
     */
    public String getHiddenWord() {
        ArrayList<String> arrayListFromWord = new ArrayList<>();

        for (int i=0; i<word.length(); i++) {
            arrayListFromWord.add(Character.toString(word.charAt(i)));

        }
        for (int i=0; i<usedLetters.size(); i++) {

            for (int j=0; j<arrayListFromWord.size(); j++) {

                if(usedLetters.get(i).equals(arrayListFromWord.get(j))) {
                    String letter = arrayListFromWord.get(j);
                    visibleWord.set(j, letter);
                }

            }

        }
        if (visibleWord.equals(arrayListFromWord)) {
            hasWon = true;

        }
        String listToString = "";
        for (int i=0; i<visibleWord.size(); i++) {
            listToString += visibleWord.get(i);

        }
        return listToString;

    }



    /**
     * guess method takes a guess as a String.
     * If guessed String letter is in word it will be added to a list of letters in the word.
     * If guessed String letter is not i the word it will be added to a list of letters used.
     *
     * @param guessed in string format
     */
    public void guess(String guessed) {
        char guess = guessed.charAt(0);

        if (word.indexOf(guess) != -1) {
            usedLetters.add(guessed);

        } else {
            badLettersUsed.add(guessed);
            triesLeft--;
        }

    }

    /**
     * getTriesleft returns the number of tries left
     *
     * @return triesLeft
     */
    public int getTriesleft() {
        if (triesLeft == 0) {
            hasLost = true;
        }
        return triesLeft;

    }

    /**
     * hasUsedLetter takes a letter as a string and checks if player has guessed letter.
     * Returns true or false
     *
     * @param c letter in string format
     * @return true if letter is in usedLetters or badLetters arraylist
     */
    public boolean hasUsedLetter(String c) {

        if(usedLetters.contains(c)) {
            return true;

        } else if (badLettersUsed.contains(c)) {
            return true;

        }
        return false;

    }

    /**
     * badLettersUsed returns a string of all the bad guessed letter.
     * Letters seperated by commas.
     *
     * @return badLettersUsed in string format
     */

    public String badLettersUsed() {
        return Arrays.toString(badLettersUsed.toArray(new String[badLettersUsed.size()])).replace("[", "").replace("]", "");
    }

    /**
     * hasLost returns true if player hast lost the game
     *
     * @return hasLost as an boolean
     */
    public boolean hasLost() {
        return hasLost;
    }

    /**
     * hasWon returns if player has won the game
     *
     * @return hasWon as an boolean
     */
    public boolean hasWon() {
        return hasWon;
    }

}




