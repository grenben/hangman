package com.example.iths.hangman;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;

/**
 * <h1>MainActivity</h1>
 * MainActivity class
 *
 * @author Johan Grenlund - ITHS.se
 * @version 2015.10.06
 * @since 1.0
 */
public class MainActivity extends AppCompatActivity {

    private Button playGameButton;
    private Button infoButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        playGameButton = (Button) findViewById(R.id.play_game_button);
        infoButton = (Button) findViewById(R.id.info_button);

       playGameButton.setOnClickListener(new View.OnClickListener() {
           @Override
            public void onClick(View v) {
               Intent intent = new Intent(v.getContext(), HangmanActivity.class);
               startActivity(intent);
           }
       });

        infoButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(v.getContext(), InfoActivity.class);
                startActivity(intent);
            }
        });

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main_activity_actions, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_info:
                Intent intentInfo = new Intent(this, InfoActivity.class);
                startActivity(intentInfo);

                return true;

            case R.id.action_play:
                Intent intentPlay = new Intent(this, HangmanActivity.class);
                startActivity(intentPlay);

                return true;

            default:

                return super.onOptionsItemSelected(item);
        }

    }

}
