package com.example.iths.hangman;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;


/**
 * <h1>ResultActivity</h1>
 * ResultActivity displays results of the Hangman game.
 * Lets user go back to main activity by pressing back button.
 *
 * @author Johan Grenlund - ITHS.se
 * @version 2015.10.06
 * @since 1.0
 */

public class ResultActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_result);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        try {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        } catch (NullPointerException e) {
            Toast.makeText(getApplicationContext(), R.string.get_support_action_bar_error_toast, Toast.LENGTH_SHORT).show();
        }

        TextView hiddenWordTextView = (TextView) findViewById(R.id.hidden_word_text_view);
        TextView numberOfTriesTextView = (TextView) findViewById(R.id.tries_left_text_view);
        TextView resultTextView = (TextView) findViewById(R.id.du_vann_text_view);

        Intent intent = getIntent();

        if (null != intent) {
            String result = intent.getStringExtra("RESULT");
            String hiddenWordString = intent.getStringExtra("HIDDEN_WORD");
            int triesLeftInt = intent.getIntExtra("TRIES_LEFT", 0);

            resultTextView.setText(result);
            hiddenWordTextView.setText(String.format(getString(R.string.result_the_hidden_word_was), hiddenWordString));
            numberOfTriesTextView.setText(String.format(getString(R.string.result_number_of_guesses_left), triesLeftInt));

        } else {
            TextView text = new TextView(this);
            text.setTextSize(40);
            text.setText(R.string.error_message);

        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu items for use in the action bar
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main_activity_actions, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_info:
                Intent intentInfo = new Intent(this, InfoActivity.class);
                startActivity(intentInfo);

                return true;

            case R.id.action_play:
                Intent intentPlay = new Intent(this, HangmanActivity.class);
                startActivity(intentPlay);
                finish();

                return true;

        }
        return super.onOptionsItemSelected(item);
    }

    /**
     * playGameButtonPressed
     * playGameButtonPressed takes user back to main activity finishing this activity.
     *
     * @param view this view
     */
    public void playGameButtonPressed(View view) {
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
        finish();

    }

}
